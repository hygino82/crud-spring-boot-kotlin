package tv.codealong.thenewboston.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/hello")
class Hello {

    @GetMapping
    fun helloTest(): String {
        return "Olá mundo kotlin";
    }
}