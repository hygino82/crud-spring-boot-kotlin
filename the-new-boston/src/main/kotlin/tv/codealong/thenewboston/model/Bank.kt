package tv.codealong.thenewboston.model

data class Bank(//cria automaticamente hashCode, equals e toString métodos
    val accountNumber: String,
    val trust: Double,
    val transactionFee: Int
)