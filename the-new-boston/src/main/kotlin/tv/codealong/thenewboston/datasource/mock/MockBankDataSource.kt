package tv.codealong.thenewboston.datasource.mock

import org.springframework.stereotype.Repository
import tv.codealong.thenewboston.datasource.BankDataSource
import tv.codealong.thenewboston.model.Bank

@Repository
class MockBankDataSource : BankDataSource {

    val banks = listOf(
        Bank("12349", 10.0, 1),
        Bank("12345", 17.3, 4),
        Bank("47856", 14.7, 3),
        Bank("31478", 9.6, 0)
    )

    override fun retrieveBanks(): Collection<Bank> = banks

}