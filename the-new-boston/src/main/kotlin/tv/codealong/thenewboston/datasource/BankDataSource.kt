package tv.codealong.thenewboston.datasource

import tv.codealong.thenewboston.model.Bank

interface BankDataSource {

    fun retrieveBanks():Collection<Bank>
}